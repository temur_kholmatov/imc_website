from django.contrib import admin
from .models import *


class WatchAdmin(admin.ModelAdmin):
    list_display = ('model', 'mainColor', 'braceletType', 'price')


class PhotoSetAdmin(admin.ModelAdmin):
    list_display = ('main', 'close', 'case', 'bottom')


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('photo',)


admin.site.register(Watch, WatchAdmin)
admin.site.register(PhotoSet, PhotoSetAdmin)
admin.site.register(Photo, PhotoAdmin)
