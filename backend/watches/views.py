from rest_framework import generics
from .serializers import WatchSerializer
from .models import Watch
from rest_framework import status
from rest_framework.response import Response


class WatchView(generics.ListAPIView):
    serializer_class = WatchSerializer
    queryset = Watch.objects.all()

    def get(self, request, *args, **kwargs):
        serializer = WatchSerializer(self.get_queryset(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class WatchByModelView(generics.ListAPIView):
    serializer_class = WatchSerializer
    queryset = Watch.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            model = kwargs['model']
        except KeyError:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        filtered_instances = self.get_queryset().filter(model=model)
        serializer = WatchSerializer(filtered_instances, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
