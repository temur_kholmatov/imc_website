from django.urls import path
from . import views


urlpatterns = [
    path('<str:model>', views.WatchByModelView.as_view()),
    path('', views.WatchView.as_view()),
]
