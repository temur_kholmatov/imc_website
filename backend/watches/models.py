from django.db import models


class Photo(models.Model):
    photo = models.ImageField(upload_to='watch_photos/%Y/%m/%d')

    def __str__(self):
        return self.photo.url


class PhotoSet(models.Model):
    main = models.ImageField(upload_to='watch_photos/%Y/%m/%d')
    close = models.ImageField(upload_to='watch_photos/%Y/%m/%d')
    case = models.ImageField(upload_to='watch_photos/%Y/%m/%d')
    carousel = models.ManyToManyField(Photo)
    bottom = models.ImageField(upload_to='watch_photos/%Y/%m/%d')


class Watch(models.Model):
    model = models.CharField(max_length=100)
    mainColor = models.CharField(max_length=50)
    color1 = models.CharField(max_length=50)
    color2 = models.CharField(max_length=50)
    braceletType = models.CharField(max_length=50)
    photo = models.ForeignKey(PhotoSet, null=True, on_delete=models.SET_NULL)
    price = models.IntegerField()
    blockColor = models.CharField(max_length=20)
