from rest_framework import serializers
from .models import *


class PhotoSetSerializer(serializers.ModelSerializer):
    carousel = serializers.StringRelatedField(many=True)

    class Meta:
        model = PhotoSet
        fields = ('main', 'close', 'case', 'carousel', 'bottom')


class WatchSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    model = serializers.CharField(max_length=100)
    mainColor = serializers.CharField(max_length=50)
    color1 = serializers.CharField(max_length=50)
    color2 = serializers.CharField(max_length=50)
    braceletType = serializers.CharField(max_length=50)
    photo = PhotoSetSerializer(allow_null=True)
    price = serializers.IntegerField()
    blockColor = serializers.CharField(max_length=20)

    def update(self, instance, validated_data):
        instance.id = validated_data.get('id', instance.id)
        instance.model = validated_data.get('model', instance.model)
        instance.mainColor = validated_data.get('mainColor', instance.mainColor)
        instance.color1 = validated_data.get('color1', instance.color1)
        instance.color2 = validated_data.get('color2', instance.color2)
        instance.braceletType = validated_data.get('braceletType', instance.braceletType)

        photo_data = validated_data.pop('photo')
        photo = instance.photo
        photo.main = photo_data.get('main', photo.main)
        photo.close = photo_data.get('close', photo.close)
        photo.case = photo_data.get('case', photo.case)
        photo.bottom = photo_data.get('bottom', photo.bottom)
        photo.save()

        instance.price = validated_data.get('price', instance.price)
        instance.blockColor = validated_data.get('blockColor', instance.blockColor)
        instance.save()
        return instance

    def create(self, validated_data):
        photoset = validated_data.pop('photo')
        photo = PhotoSet.objects.create(**photoset)
        return Watch.objects.create(**validated_data, photo=photo)
