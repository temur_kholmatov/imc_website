from django.contrib import admin
from .models import EmailSubmission


class EmailSubmissionAdmin(admin.ModelAdmin):
    list_display = ('email',)


admin.site.register(EmailSubmission, EmailSubmissionAdmin)
