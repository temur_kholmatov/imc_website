from django.db import models


class EmailSubmission(models.Model):
    email = models.EmailField()
