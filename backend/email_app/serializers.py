from rest_framework import serializers
from .models import EmailSubmission


class EmailSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailSubmission
        fields = ('email',)
