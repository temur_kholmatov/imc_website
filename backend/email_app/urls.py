from django.urls import path
from . import views


urlpatterns = [
    path('', views.EmailSubmissionView.as_view()),
]
