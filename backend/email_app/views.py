from rest_framework import generics
from .serializers import EmailSubmissionSerializer
from .models import EmailSubmission
from rest_framework import status
from rest_framework.response import Response


class EmailSubmissionView(generics.CreateAPIView):
    serializer_class = EmailSubmissionSerializer
    queryset = EmailSubmission.objects.all()

    def post(self, request, *args, **kwargs):
        if not self.get_queryset().filter(email=request.data['email']).exists():
            super().post(request, *args, **kwargs)
            return Response(status.HTTP_202_ACCEPTED)
        else:
            return Response(status.HTTP_200_OK)
