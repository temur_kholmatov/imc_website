import React from 'react';
import Header from '../common/Header'
import Footer from '../common/Footer'
import MenComponent from '../components/MenComponent/Men'
import actions from "../redux/actions";
import {Spinner} from 'react-bootstrap'

import {connect} from 'react-redux'
import * as styles from "../components/MenComponent/Men.module.scss";

class Men extends React.Component {

    componentDidMount() {
        this.props.getWatchesList();
    }

    render() {
        if (!this.props.list) return <Spinner animation="border" role="status" className={styles.spinner}/>;
        return (
            <div>
                <Header/>
                <MenComponent list={this.props.list} color={this.props.color}
                              getWatchesList={this.props.getWatchesList} setColor={this.props.setColor}/>
                <Footer/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        list: state.watch.list,
        color: state.color.color,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getWatchesList: () => dispatch(actions.watchesActions.getWatchesList()),
        setColor: (color) => dispatch(actions.colorActions.setColor(color)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Men);