import React from 'react';
import {Route} from 'react-router'
import Header from '../common/Header'
import OrderComponents from '../components/OrderComponent'

class Order extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <Route exact path='/order/' component={OrderComponents.Order}/>
                <Route path='/order/delivery' component={OrderComponents.Delivery}/>
                <Route exact path='/order/payment' component={OrderComponents.Payment}/>
                <Route path='/order/done' component={OrderComponents.Done}/>
            </div>
        )
    }
}

export default Order;