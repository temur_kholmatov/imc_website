import React from 'react';
import logo from '../images/IMC_logo.png'
import inst from '../images/socialIcons/Instagram_icon.svg'
import vk from '../images/socialIcons/Vk_icon.svg'
import * as styles from './Error.module.scss'
import axios from "axios";

class ErrorPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: true
        }
    }

    async animation(e) {
        const email = document.getElementById('emailInput').value;
        let bodyFormData = new FormData();
        bodyFormData.append('email', email);
        const url = `http://localhost:8000/`;
        let res = await axios.post(url, bodyFormData);

        e.preventDefault();
        if (this.state.animation) {
            document.getElementById('submitButton').innerHTML = '';
            document.getElementById('submitButton').className = 'animation';
            document.getElementById('emailInput').className = 'animation2';
            setTimeout(() => document.getElementById('submitButton').innerHTML = 'УСПЕШНО.', 3000);
            this.setState({animation: false})
        }
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.content}>
                    <img className={styles.logo} src={logo} alt='Logo'/>
                    <h1 className={styles.errorStatus}>404</h1>
                    <form className={styles.emailForm}>
                        <input className={styles.emailInput} id='emailInput' type='email' placeholder='Введите e-mail'/>
                        <button className={styles.submitButton} id='submitButton' type='submit'
                                onClick={e => this.animation(e)}>ОТПРАВИТЬ
                        </button>
                    </form>
                </div>
                <div className={styles.footer}>
                    <div className={styles.text}>
                        <p className={styles.firstHeader}>Сайт скоро снова появится.</p>

                        <p className={styles.secondHeader}>Подпишитесь на нашу e-mail рассылку, чтобы первыми получать все
                            новости, узнавать о скидках и получать подарки.</p>
                    </div>
                    <div className={styles.socialIcons}>
                        <a href='https://www.instagram.com/imcwatches/' target="_blank"  rel="noopener noreferrer">
                            <img  src={inst} alt='Instagram'/>
                        </a>
                        <a href='https://vk.com/imcwatches' target="_blank"  rel="noopener noreferrer">
                            <img className={styles.vk} src={vk} alt='VKontakte'/>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorPage;