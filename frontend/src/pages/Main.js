import React from 'react'
import Header from '../common/Header'
import Footer from '../common/Footer'
import MainComponent from '../components/Main'
import actions from '../redux/actions'
import {connect} from 'react-redux'
import {Spinner} from 'react-bootstrap'


class Main extends React.Component {
    componentDidMount() {
        let model = window.location.href.split('/');
        model = model[model.length - 1];
        this.props.getWatchModel(model);
    }

    getColors() {
        let colors = [];
        this.props.model.forEach(item => {
            if (!colors.includes(item.mainColor)) colors.push(item.mainColor)
        });
        return colors
    }

    render() {
        if (!this.props.model) return <Spinner/>;
        else {
            const colors = this.getColors();
            return (
                <div style={{margin: 0, display: 'flex', flexDirection: 'column'}}>
                    <Header/>
                    <MainComponent.TopBlock model={this.props.model} color={this.props.color} colors={colors}
                                            setColor={this.props.setColor}/>
                    <MainComponent.MiddleBlock model={this.props.model} color={this.props.color} colors={colors}/>
                    <MainComponent.BottomBlock model={this.props.model} color={this.props.color} colors={colors}/>
                    <MainComponent.EmailForm model={this.props.model} color={this.props.color} colors={colors}/>
                    <Footer/>
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        model: state.watch.model,
        color: state.color.color
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setColor: (color) => dispatch(actions.colorActions.setColor(color)),
        getWatchModel: (model) => dispatch(actions.watchesActions.getWatchModel(model))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);