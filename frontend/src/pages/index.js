import Main from './Main';
import Error from './Error'
import Order from './Order'
import Men from './Men'

export default {
    Main,
    Error,
    Order,
    Men
}