import React from 'react';
import * as styles from './TopBlock.module.scss'
import vk from '../../images/socialIcons/Vk_dark_icon.svg'
import facebook from '../../images/socialIcons/Facebook_icon.svg'
import twitter from '../../images/socialIcons/Twitter_icon.svg'
import chooseColor from '../../common/params'

import {Link} from 'react-router-dom'

class TopBlock extends React.Component {
    state = {
        selected: 0,
    };

    getColors(model) {
        let colors = [];
        const currentColor = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])].mainColor;

        model.forEach(item => {
            if (!colors.includes(item.mainColor)) colors.push(item.mainColor)
        });

        colors.splice(colors.indexOf(currentColor), 1);
        colors.splice(0, 0, currentColor);
        return colors;
    }

    addToOrder() {
        const arr = JSON.parse(localStorage.getItem('cart'));
        const model = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])];
        const colors = this.getColors(this.props.model);
        if (arr) {
            if (arr.find(item => item.model.id === model.id)) {
                for (let i = 0; i < arr.length; i++) {
                    if (arr[i].model.id === model.id) {
                        arr[i].count += 1;
                    }
                }
            } else arr.push({model: model, count: 1, colors});

            localStorage.setItem('cart', JSON.stringify(arr))

        } else localStorage.setItem('cart', JSON.stringify([{model: model, count: 1, colors}]));
        const totalPrice = localStorage.getItem('total');
        totalPrice ? localStorage.setItem('total', (+totalPrice + +model.price)) : localStorage.setItem('total', model.price)

    }

    render() {
        const model = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])];
        return (
            <div className={styles.topBlock}>
                <div className={styles.leftPanel}>
                    <img className={this.state.selected === 0 ? styles.selected : styles.notSelected}
                         src={model.photo.main}
                         alt='Главная' onClick={() => this.setState({selected: 0})}/>
                    <img className={this.state.selected === 1 ? styles.selected : styles.notSelected}
                         src={model.photo.close}
                         alt='Главная' onClick={() => this.setState({selected: 1})}/>
                    <img className={this.state.selected === 2 ? styles.selected : styles.notSelected}
                         src={model.photo.case}
                         alt='Главная' onClick={() => this.setState({selected: 2})}/>
                </div>

                <img className={styles.selectedBig}
                     src={this.state.selected === 0 ? model.photo.main : (this.state.selected === 1 ? model.photo.close : model.photo.case)}
                     alt='Главня'/>

                <div className={styles.rightPanel}>
                    <p className={styles.title}>IMC {model.model}</p>
                    <div className={styles.parameters}>
                        <p className={styles.colorsLabel}>Цвет:
                            <span className={styles.colors}>
                                {this.props.colors.map(color => (
                                    <img
                                        className={(this.props.color ? this.props.color : this.props.colors[0]) === color ? styles.selectedColor : styles.notSelectedColor}
                                        src={chooseColor.chooseColor(color)}
                                        alt={color} key={color} onClick={() => this.props.setColor(color)}/>
                                ))}
                            </span>
                        </p>
                        <p className={styles.parameterLabel}>Размер:<span
                            className={styles.parameterValue}>40 мм</span>
                        </p>
                        <p className={styles.parameterLabel}>Цена:<span
                            className={styles.parameterValue}>{model.price.toLocaleString().replace(',', ' ')} RUB</span>
                        </p>
                    </div>
                    <p className={styles.description}>
                        Lorem ipsum dolor sit amet, consetetur<br/>
                        sadipscing elitr, sed diam nonumy eirmod tempor<br/>
                        invidunt ut labore et dolore magna aliquyam<br/>
                        erat, sed diam voluptua. At vero eos et accusam<br/>
                        et justo duo dolores et ea rebum. Stet clita kasd
                    </p>

                    <Link to={'/order'}>
                        <button className={styles.buy} onClick={() => this.addToOrder()}><span
                            className={styles.buyText}>КУПИТЬ</span></button>
                    </Link>

                    <div className={styles.share}>
                        <p className={styles.shareText}>Поделиться через</p>
                        <div className={styles.shareIcons}>
                            <img src={vk} className={styles.shareIconVk} alt='asd'/>
                            <img src={facebook} className={styles.shareIconFacebook} alt='asd'/>
                            <img src={twitter} className={styles.shareIconTwitter} alt='asd'/>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}


export default TopBlock;