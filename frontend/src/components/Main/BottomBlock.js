import React from 'react';
import * as styles from './BottomBlock.module.scss'

class BottomBlock extends React.Component {

    render() {
        const model = this.props.model[this.props.colors.indexOf(this.props.color ? this.props.color : this.props.colors[0])];

        return (
            <div className={styles.bottomBlock}>
                <div className={styles.leftPanel}>
                    <p className={styles.title}>кварцевый механизм</p>
                    <span className={styles.description}>citizen miyota quartz</span>

                    <p className={styles.title}>стальной корпус</p>
                    <span className={styles.description}>никакого аллергического воздействия</span>

                    <p className={styles.title}>размер</p>
                    <span className={styles.description}>толщина 9 мм</span>
                    <span className={styles.description}>диаметр 40 мм</span>

                    <p className={styles.title}>тахометрическая шкала</p>
                    <span className={styles.description}>позволяет измерять</span>
                    <span className={styles.description}>среднюю скорость </span>
                    <span className={styles.description}>движения</span>

                </div>
                <div className={styles.border} style={{backgroundColor: model.blockColor}}/>
                <img className={styles.bottom}
                     src={model.photo.bottom}
                     alt='asd'/>
            </div>
        )
    }
}

export default BottomBlock;