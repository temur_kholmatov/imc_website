import TopBlock from './TopBlock'
import MiddleBlock from './MiddleBlock'
import BottomBlock from './BottomBlock'
import EmailForm from './EmailForm'

export default {
    TopBlock,
    MiddleBlock,
    BottomBlock,
    EmailForm
}