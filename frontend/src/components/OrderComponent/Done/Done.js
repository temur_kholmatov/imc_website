import React from 'react';
import * as styles from './Done.module.scss'
import status from "../../../images/statusDone.png";
import {Col, Row} from "react-bootstrap";

class Done extends React.Component {

    render() {
        const address = JSON.parse(localStorage.getItem('address'));
        const item = JSON.parse(localStorage.getItem('orders'))[0];
        const order = item.order, totalPrice = item.price;
        return (
            <div className={styles.done}>
                <div className={styles.header}>
                    <div style={{paddingLeft: '373px', width: '100%'}}>
                        <h1 className={styles.title}>ОПЛАТА</h1>
                        <h3 className={styles.description}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/>
                            sed diam nonumy eirmod tempor invidunt ut labore et</h3>
                    </div>
                    <img src={status} alt={'status'} className={styles.status}/>
                </div>

                <Row className={styles.content}>

                    <Col className={styles.block}>
                        <div className={styles.msg}>
                            <h1 className={styles.title}>ВАШ ЗАКАЗ ОФОРМЛЕН</h1>
                            <p className={styles.description}>Мы готовим его к отправке.</p>
                        </div>
                    </Col>


                    <Col className={styles.order}>
                        <div className={styles.address}>
                            <h1 className={styles.addressHeader}>Адрес доставки</h1>
                            <div className={styles.addressData}>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.name}</p>
                                    <p className={styles.addressField}>{address.surname}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressFieldFull}>{address.email}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.country}</p>
                                    <p className={styles.addressField}>{address.city}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressField}>{address.index}</p>
                                    <p className={styles.addressField}>{address.phone}</p>
                                </Row>
                                <Row className={styles.addressRow}>
                                    <p className={styles.addressFieldFull}>{address.address}</p>
                                </Row>
                            </div>
                        </div>

                        {order.map(item => (
                            <div className={styles.item} key={item.model.id}>
                                <img src={item.model.photo.main} alt={'watch'} className={styles.watch}/>
                                <div className={styles.modelDescription}>
                                    <p className={styles.model}>{item.model.model}</p>
                                    <p className={styles.modelSex}>Мужские часы</p>
                                </div>
                                <p className={styles.price}>{(item.model.price * item.count).toLocaleString().replace(/,/g, ' ')} RUB</p>
                            </div>
                        ))}


                        <div className={styles.total}>
                            <p className={styles.totalText}>Всего</p>
                            <p className={styles.totalPrice}>{(+totalPrice).toLocaleString().replace(/,/g, ' ')} RUB</p>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Done;