import React, {Component} from 'react'
import {Slider, Rail, Handles, Tracks} from 'react-compound-slider'
import {SliderRail, Handle, Track} from './SliderCustomization' // example render components - source below

const sliderStyle = {
    position: 'relative',
    width: '100%',
};


class SliderComp extends Component {
    state = {
        domain: [0, this.props.max],
        values: this.props.values,
        update: this.props.values,
        reversed: false,
    };


    render() {
        const {
            state: {domain, values, reversed},
        } = this;

        return (
            <div style={{height: 30, width: 129, marginTop: 22, marginLeft: 15}}>
                <Slider
                    mode={1}
                    step={100}
                    domain={domain}
                    reversed={reversed}
                    rootStyle={sliderStyle}
                    onUpdate={this.props.onUpdate}
                    onChange={this.props.onChange}
                    values={values}
                >
                    <Rail>
                        {({getRailProps}) => <SliderRail getRailProps={getRailProps}/>}
                    </Rail>
                    <Handles>
                        {({handles, getHandleProps}) => (
                            <div className="slider-handles">
                                {handles.map(handle => (
                                    <Handle
                                        key={handle.id}
                                        handle={handle}
                                        domain={domain}
                                        getHandleProps={getHandleProps}
                                    />
                                ))}
                            </div>
                        )}
                    </Handles>
                    <Tracks left={false} right={false}>
                        {({tracks, getTrackProps}) => (
                            <div className="slider-tracks">
                                {tracks.map(({id, source, target}) => (
                                    <Track
                                        key={id}
                                        source={source}
                                        target={target}
                                        getTrackProps={getTrackProps}
                                    />
                                ))}
                            </div>
                        )}
                    </Tracks>

                </Slider>
            </div>
        )
    }
}

export default SliderComp;