import React from 'react'
import * as styles from './Header.module.scss'
import logo from '../images/IMC_logo.png'
import cart from '../images/header/cart.svg'
import search from '../images/header/search.svg'
import rus from '../images/header/rus.svg'


const menuItems = [{title: 'ГЛАВНАЯ', link: '/men'}, {title: 'ДЛЯ НЕГО', link: '/men'}, {
    title: 'ДЛЯ НЕЁ',
    link: '/men'
}, {title: 'О НАС', link: '/men'}, {title: 'БЛОГ', link: '/men'}];

class Header extends React.Component {
    state = {search: false};

    render() {
        return (
            <div className={styles.header}>
                <img className={styles.logo} src={logo} alt='Logo'/>
                <ul className={styles.menu}>
                    {menuItems.map(item => (
                        <li key={item.title} className={styles.menuItem}>
                            <a href={'/men'}>{item.title}</a>
                        </li>
                    ))}
                </ul>
                <div className={styles.icons}>
                    <img src={rus} alt='Язык'/>
                    <img src={search} alt='Поиск' onClick={() => this.setState({search: !this.state.search})}/>
                    <img src={cart} alt='Корзина'/>
                </div>
                {this.state.search &&
                <form>
                    <input type={'text'} className={styles.search}/>
                </form>
                }
            </div>
        )
    }
}

export default Header;