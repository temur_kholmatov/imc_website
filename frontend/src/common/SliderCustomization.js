import React, {Fragment} from 'react'

// *******************************************************
// RAIL
// *******************************************************
const railOuterStyle = {
    position: 'absolute',
    width: '100%',
    height: 30,
    // transform: 'translate(0%, -50%)',
    borderRadius: 10,
    cursor: 'pointer',
    // border: '1px solid white',
};

const railInnerStyle = {
    position: 'absolute',
    width: '100%',
    height: 20,
    // transform: 'translate(0%, -50%)',
    borderRadius: 10,
    pointerEvents: 'none',
    backgroundColor: '#acacac',
};

export function SliderRail({getRailProps}) {
    return (
        <Fragment>
            <div style={railOuterStyle} {...getRailProps()} />
            <div style={railInnerStyle}/>
        </Fragment>
    )
}


// *******************************************************
// HANDLE COMPONENT
// *******************************************************
export function Handle({
                           domain: [min, max],
                           handle: {id, value, percent},
                           disabled,
                           getHandleProps,
                       }) {
    return (
        <Fragment>
            <div
                style={{
                    left: `${percent}%`,
                    position: 'absolute',
                    transform: 'translate(-50%, 0)',
                    WebkitTapHighlightColor: 'rgba(0,0,0,0)',
                    zIndex: 5,
                    width: 28,
                    height: 30,
                    cursor: 'pointer',
                    // border: '1px solid white',
                    backgroundColor: 'none',
                }}
                {...getHandleProps(id)}
            />
            <div
                role="slider"
                aria-valuemin={min}
                aria-valuemax={max}
                aria-valuenow={value}
                style={{
                    left: `${percent}%`,
                    position: 'absolute',
                    transform: 'translate(-50%, -5px)',
                    zIndex: 2,
                    width: 30,
                    height: 30,
                    borderRadius: '50%',
                    boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.3)',
                    backgroundColor: disabled ? '#666' : '#ebebeb',
                }}
            />
        </Fragment>
    )
}


// *******************************************************
// KEYBOARD HANDLE COMPONENT
// Uses a button to allow keyboard events
// *******************************************************
// export function KeyboardHandle({
//                                    domain: [min, max],
//                                    handle: {id, value, percent},
//                                    disabled,
//                                    getHandleProps,
//                                }) {
//     return (
//         <button
//             role="slider"
//             aria-valuemin={min}
//             aria-valuemax={max}
//             aria-valuenow={value}
//             style={{
//                 left: `${percent}%`,
//                 position: 'absolute',
//                 transform: 'translate(-50%)',
//                 zIndex: 2,
//                 width: 24,
//                 height: 24,
//                 borderRadius: '50%',
//                 boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.3)',
//                 backgroundColor: disabled ? '#666' : '#ffc400',
//             }}
//             {...getHandleProps(id)}
//         />
//     )
// }


// *******************************************************
// TRACK COMPONENT
// *******************************************************
export function Track({source, target, getTrackProps, disabled}) {
    return (
        <div
            style={{
                position: 'absolute',
                transform: 'translate(0%, 0)',
                height: 20,
                zIndex: 1,
                backgroundColor: disabled ? '#999' : '#acacac',
                borderRadius: 14,
                cursor: 'pointer',
                left: `${source.percent}%`,
                width: `${target.percent - source.percent}%`,
            }}
            {...getTrackProps()}
        />
    )
}


// *******************************************************
// TICK COMPONENT
// *******************************************************
// export function Tick({tick, count, format}) {
//     return (
//         <div>
//             <div
//                 style={{
//                     position: 'absolute',
//                     marginTop: 14,
//                     width: 1,
//                     height: 5,
//                     backgroundColor: 'rgb(200,200,200)',
//                     left: `${tick.percent}%`,
//                 }}
//             />
//             <div
//                 style={{
//                     position: 'absolute',
//                     marginTop: 22,
//                     fontSize: 10,
//                     textAlign: 'center',
//                     marginLeft: `${-(100 / count) / 2}%`,
//                     width: `${100 / count}%`,
//                     left: `${tick.percent}%`,
//                 }}
//             >
//                 {format(tick.value)}
//             </div>
//         </div>
//     )
// }
