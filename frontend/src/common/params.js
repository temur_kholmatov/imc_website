import aqua from '../images/watches/colors/Aqua.svg'
import black from '../images/watches/colors/Black.svg'
import blue from '../images/watches/colors/Blue.svg'
import brown from '../images/watches/colors/Brown.svg'
import gold from '../images/watches/colors/Gold.svg'
import grey from '../images/watches/colors/Grey.svg'
import ivory from '../images/watches/colors/Ivory.svg'
import silver from '../images/watches/colors/Silver.svg'
import silverBlack from '../images/watches/colors/SilverBlack.svg'
import goldLeather from '../images/watches/colors/GoldLeather.svg'
import silverLeather from '../images/watches/colors/SilverLeather.svg'

function chooseColor(color) {
    switch (color) {
        case 'Aqua':
            return aqua;
        case 'Black':
            return black;
        case 'Blue':
            return blue;
        case 'Brown':
            return brown;
        case 'Gold':
            return gold;
        case 'Grey':
            return grey;
        case 'Ivory':
            return ivory;
        case 'Silver':
            return silver;
        case 'Silver/Black':
            return silverBlack;
        case 'Gold/Leather':
            return goldLeather;
        case 'Silver/Leather':
            return silverLeather;
        default:
            return null;
    }
}

function chooseBracelet(type) {
    switch (type) {
        case 'Metal':
            return 'металлический';
        case 'Leather':
            return 'кожаный';
        default:
            return type;
    }
}


function blockColor(watch) {
    switch (watch) {
        case 'Iconic_Aqua':
            return aqua;
        case 'Black':
            return black;
        case 'Blue':
            return blue;
        case 'Brown':
            return brown;
        case 'Gold':
            return gold;
        case 'Grey':
            return grey;
        case 'Ivory':
            return ivory;
        case 'Silver':
            return silver;
        case 'Silver/Black':
            return silverBlack;
        default:
            return null;
    }
}

export default {
    chooseColor,
    chooseBracelet,
    blockColor
}