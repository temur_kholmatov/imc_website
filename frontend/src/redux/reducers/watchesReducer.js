import constants from '../constants';

const initialState = [];


export const watch = (state = initialState, action) => {
    switch (action.type) {
        case constants.watchesList.GET_WATHCES_LIST + constants.requestStatus.SUCCESS:
            return {
                list: action.payload
            };
        case constants.watchModel.GET_WATCH_MODEL + constants.requestStatus.SUCCESS:
            return {
                model: action.payload
            };
        default:
            return state;
    }
};


