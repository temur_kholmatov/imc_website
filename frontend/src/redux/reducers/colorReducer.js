import constants from '../constants';

const initialState = [];


export const color = (state = initialState, action) => {
    switch (action.type) {
        case constants.currentColor.SET_COLOR:
            return {
                color: action.color
            };
        default:
            return state;
    }
};


