import constants from '../constants';
import utils from './utils';
import watches from '../../common/api/watches'

const {request, success, failure} = utils;


function getWatchesList() {
    return (dispatch) => {
        dispatch(request(constants.watchesList.GET_WATHCES_LIST));
        watches.getWatchesList().then(res => {
            dispatch(success(constants.watchesList.GET_WATHCES_LIST, res));
        }).catch(err => {
            dispatch(failure(constants.watchesList.GET_WATHCES_LIST, err));
        })
    }
}

function getWatchModel(model) {
    return (dispatch) => {
        dispatch(request(constants.watchModel.GET_WATCH_MODEL));
        watches.getWatchModel(model).then(res => {
            dispatch(success(constants.watchModel.GET_WATCH_MODEL, res));
        }).catch(err => {
            dispatch(failure(constants.watchModel.GET_WATCH_MODEL, err));
        })
    }
}


export default {
    getWatchesList,
    getWatchModel,
}