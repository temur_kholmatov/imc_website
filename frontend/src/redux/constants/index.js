const watchesList = {
    GET_WATHCES_LIST: 'GET_WATHCES_LIST',
};

const watchModel = {
    GET_WATCH_MODEL: 'GET_WATCH_MODEL'
};

const currentColor = {
    SET_COLOR: 'SET_COLOR'
};

const requestStatus = {
    REQUEST: "_REQUEST",
    SUCCESS: "_SUCCESS",
    FAILURE: "_FAILURE"
};


export default {
    requestStatus,
    watchesList,
    currentColor,
    watchModel
}