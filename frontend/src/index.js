import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import './index.css';
// import App from './App';
import {Router} from 'react-router-dom';
import {Route, Switch} from 'react-router';
import * as serviceWorker from './serviceWorker';
import {store} from './redux/store';
// import Pages from './pages'
import Error from './pages/Error'
import history from './history'

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Switch>
                {/*<Route exact path='/' component={App}/>*/}
                <Route exact path='/' component={Error}/>
                {/*<Route path='/order' component={Pages.Order}/>*/}
                {/*<Route path='/men' component={Pages.Men}/>*/}
                {/*<Route path='/watch' component={Pages.Main}/>*/}
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
